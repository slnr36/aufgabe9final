let benutzeriste: string [] = [];
let currentuser: string = "";
let currentindex: number = 0;

function benutzerhin () {
    const input: JQuery = $("#Benutzername");
    let name: string = input.val().toString();
    benutzeriste.push(name);
    renderlist();
}

function renderlist () {
    let list: JQuery = $("#userlist");
    list.empty();
    for (let i: number = 1; i <= benutzeriste.length; i++) {
        let row: JQuery = $("<div class=`col`>" + benutzeriste [i] + "</div>");
        let button: JQuery = $("<div class=`col-1`></div>").on("click", () => {
                this.openedit(benutzeriste[i], i);
            }
        );
        list.append(row);
        list.append(button);
    }
}

function openedit (name: string, index: number) {
    let editmodal: JQuery = $("#edit");
    editmodal.modal();
    currentindex = index;
    currentuser = name;

}

function useredit () {
    let editinput : JQuery = $("#editinput");
    currentuser = editinput.val().toString();
    benutzeriste[currentindex] = currentuser;
    renderlist();
    editinput.val("");
    let editmodal: JQuery = $("#edit");
    editmodal.modal ("hide");
}

$( () => {
    const benutzeradd: JQuery = $("#benutzerhinzufügen");
    const benutzeredit: JQuery = $("#editsave");

    benutzeradd.on("click", benutzerhin);
    benutzeredit.on("click", useredit);
});